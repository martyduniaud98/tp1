# TP

**1 - Delete un dossier important**

- Simplement delete le dossier boot, pour ne plus pouvoir démarrer la VM, avec le bloc de code suivant : 

```
marty@marty-VirtualBox:/$ sudo rm -r boot
[sudo] password for marty:
rm: cannot remove 'boot/efi': Device or resource busy
marty@marty-VirtualBox:/$
``` 
Résultat obtenu lors du redémarrage de la VM : 
```
error: file `/boot/grub/i386-pc/normal.mod' not found.
Entering rescue mode...
grub rescue>
```

<br><br>

**2 - Fork Bomb**

- J'ai crée un script avec la commande ci-dessous, puis j'ai mis les droits d'execut pour pouvoir le lancer automatiquement au démarrage
```
#!/bin/bash
:(){ :|:& };:
```
cette commande est une fonction qui permet de se rappeler de façon récursive pour pouvoir manger indéfiniment les ressources système &#x1F924;
La VM se lancera mais crashera presque instantanément

<br>

**3 - Les droits à 000**

- J'ai mis les droits minimum d'accès sur le dossier /bin . 
```
marty@marty-VirtualBox:/$ sudo chmod 777 /bin
bash: /usr/bin/sudo: Permission denied
```
Je ne peux plus utiliser certaines commandes, ni même changer les droits &#x1F642;

<br>

**4 - lock périphériques**

- On va lock la sourie et le clavier en utilisant un script au démarrage via la commande suivante :

```
sudo apt-get install xtrlock
``` 
il ne reste plus qu'à insérer la commande ```xtrlock``` dans le script.

<br>

**5 - Shutdown System**

- Ici on va créer un script au démarrage permettant d'éteindre le système grâce à la commande ```shutdown -h now``` lors du démarrage du système après s'être log, je ne peux donc plus utiliser ma VM &#x1F62D;

``` 
#Nous sommes dans le script
#!/bin/bah
shutdown -h now
```

